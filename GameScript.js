let p_Rock = document.getElementById("pBatu"); //id div element rock
let p_Papper = document.getElementById("pKertas"); //id div element papper
let p_Scissor = document.getElementById("pGunting"); //id div element scissor
let b_Rock = document.getElementById("bBatu");
let b_Papper = document.getElementById("bKertas");
let b_Scissor = document.getElementById("bGunting");
let p_Picked = "";
let b_Picked = "";
let winner = "";
let player = "Player 1";
let bot = "Com";
let _refresh = document.getElementById("refresh");
let vsText = document.getElementById("vsText");
let p1Winner = document.getElementById("pWinText");
let comWinner = document.getElementById("bWinText");
let draw = document.getElementById("drawText");

//Player UI
//Event Click
p_Rock.addEventListener("click", function () {
  PlayerPick("batu"), botBrain(), gameplay();
  vsText.innerHTML = "";
});
p_Papper.addEventListener("click", function () {
  PlayerPick("kertas"), botBrain(), gameplay();
});
p_Scissor.addEventListener("click", function () {
  PlayerPick("gunting"), botBrain(), gameplay();
});
_refresh.addEventListener("click", function () {
  PlayerPick("");
});

//Styling and locking player choosen option
function PlayerPick(choice) {
  if (choice == "batu") {
    // if choose rock then :
    p_Rock.style.backgroundColor = "red";
    p_Papper.classList.remove("hover");
    p_Scissor.classList.remove("hover");
    p_Papper = "";
    p_Scissor = "";
    p_Picked = choice;
  } else if (choice == "kertas") {
    // if chosse papper then :
    p_Papper.style.backgroundColor = "blue";
    p_Rock.classList.remove("hover");
    p_Scissor.classList.remove("hover");
    p_Rock = "";
    p_Scissor = "";
    p_Picked = choice;
  } else if (choice == "gunting") {
    // if choose scissor then :
    p_Scissor.style.backgroundColor = "yellow";
    p_Rock.classList.remove("hover");
    p_Papper.classList.remove("hover");
    p_Papper = "";
    p_Rock = "";
    p_Picked = choice;
  } else if (choice == "") {
    location.reload();
  }
  console.log("Player_" + p_Picked);
}

function botBrain() {
  const option = ["batu", "kertas", "gunting"];
  b_Picked = option[Math.floor(Math.random() * option.length)];
  console.log("bot_" + b_Picked);
  botPick(b_Picked);
  return b_Picked;
}

function botPick(choice) {
  if (choice == "batu") {
    b_Rock.style.backgroundColor = "red";
  } else if (choice == "kertas") {
    b_Papper.style.backgroundColor = "blue";
    return botPick;
  } else if (choice == "gunting") {
    b_Scissor.style.backgroundColor = "yellow";
    return botPick;
  } else {
  }
}

function gameplay() {
  if (b_Picked == "kertas" && p_Picked == "gunting") {
    p1Win();
    return (winner = player);
  } else if (b_Picked == "kertas" && p_Picked == "batu") {
    comWin();
    return (winner = bot);
  } else if (b_Picked == "gunting" && p_Picked == "kertas") {
    comWin();
    return (winner = bot);
  } else if (b_Picked == "gunting" && p_Picked == "batu") {
    p1Win();
    return (winner = player);
  } else if (b_Picked == "batu" && p_Picked == "kertas") {
    p1Win();
    return (winner = player);
  } else if (b_Picked == "batu" && p_Picked == "gunting") {
    comWin();
    return (winner = bot);
  } else {
    drawWin();
    return (winner = "Draw");
  }
}
//mid Text Style handler
function p1Win() {
  p1Winner.style.visibility = "visible";
  comWinner.style.visibility = "hidden";
  vsText.style.visibility = "hidden";
  draw.style.visibility = "hidden";
}

function comWin() {
  p1Winner.style.visibility = "hidden";
  comWinner.style.visibility = "visible";
  vsText.style.visibility = "hidden";
  draw.style.visibility = "hidden";
}

function drawWin() {
  p1Winner.style.visibility = "hidden";
  comWinner.style.visibility = "hidden";
  vsText.style.visibility = "hidden";
  draw.style.visibility = "visible";
}
p1Winner.style.visibility = "hidden";
comWinner.style.visibility = "hidden";
vsText.style.visibility = "visible";
draw.style.visibility = "hidden";
